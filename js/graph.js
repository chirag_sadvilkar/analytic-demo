$(document).ready(function(){
    $('#select-company').on('change', function(){
     var demovalue = $(this).val(); 
        $("div.sales-graph").hide();
        $("#show"+demovalue).show();
    });
});

/* Graph Section */



/* Sales pipeline Graph Section -- Total value of contracts increased */ 
$(function(){
    var productsToValues = function (){ 
        return $.map(products, function (item){
            return item.active ? item.count : null;
        });
    };

    var gauge = $("#gauge").dxBarGauge({
        startValue: 0,
        endValue: 100,
        values: productsToValues(),
        label: {
            format: {
                type: "fixedPoint",
                precision: 0
            }
        }
    }).dxBarGauge("instance");
    
    $("#panel").append($.map(products, function (product) {
        return $("<div></div>").dxCheckBox({
            value: product.active,
            text: product.name,
            onValueChanged: function(data) {        
                product.active = data.value;
                gauge.values(productsToValues());
            }    
        });
    }));



});

$(function(){
    $("#gauge").dxBarGauge({
        startValue: 0,
        endValue: 100,
        values: [41, 32, 13, 48, 24],
        label: {
            format: {
                type: "fixedPoint",
                precision: 0
            }
        },
        tooltip: {
            enabled: true,
            customizeTooltip: function (arg) {
                return {
                    text: getText(arg, arg.valueText)
                };
            }
        },
        "export": {
            enabled: true
        },
        legend: {
            visible: true,
            verticalAlignment: "bottom",
            horizontalAlignment: "center",
            customizeText: function(arg) {
                return getText(arg.item, arg.text);
            }
        }
    });

    function getText(item, text){
        return "name " + (item.index + 1) + " - " + text + " km/h";
    }
});

var products = [{
    name: 'self-awareness',
    count: 41,
    active: true
}, {
    name: 'self-confidence',
    count: 32,
    active: true
}, {
    name: 'motivation',
    count: 13,
    active: true
}, {
    name: 'communication',
    count: 48,
    active: true
}, {
    name: 'building relations',
    count: 24,
    active: true
}];




/* Sales pipeline Graph Section -- Total number of contracts increased */ 


    $(function(){
    var productsToValues = function (){ 
        return $.map(products1, function (item){
            return item.active ? item.count : null;
        });
    };

    var gauge = $("#gauge1").dxBarGauge({
        startValue: 0,
        endValue: 100,
        values: productsToValues(),
        label: {
            format: {
                type: "fixedPoint",
                precision: 0
            }
        }
    }).dxBarGauge("instance");
    
    $("#panel1").append($.map(products1, function (product) {
        return $("<div></div>").dxCheckBox({
            value: product.active,
            text: product.name,
            onValueChanged: function(data) {        
                product.active = data.value;
                gauge.values(productsToValues());
            }    
        });
    }));



});

var products1 = [{
    name: 'self-awareness',
    count: 21,
    active: true
}, {
    name: 'self-confidence',
    count: 42,
    active: true
}, {
    name: 'motivation',
    count: 23,
    active: true
}, {
    name: 'communication',
    count: 38,
    active: true
}, {
    name: 'building relations',
    count: 44,
    active: true
}];

